import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = element.querySelector('form');
		const input = form.querySelector("input[name='name'");
		form.addEventListener('submit', e => this.submit(e, input));
	}

	submit(event, input) {
		event.preventDefault();
		console.log(input.value);
		if (input.value === '') alert('C vide la');
		else alert(`La pizza ${input.value} a été ajoutée`);
	}
}
