export default class Router {
	static titleElement;
	static contentElement;
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 */

	static #menuElement; // propriété statique privée
	/**
	 * Indique au Router la balise HTML contenant le menu de navigation
	 * Écoute le clic sur chaque lien et déclenche la méthode navigate
	 * @param element Élément HTML qui contient le menu principal
	 */

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			// affichage du titre de la page
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			// affichage de la page elle même
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}
	static set menuElement(element) {
		this.#menuElement = element;
		const listElement = this.#menuElement.querySelectorAll('a');

		listElement.forEach(element => {
			element.addEventListener('click', e => {
				console.log(e);
				e.preventDefault();
				Router.navigate(e.target.getAttribute('href'));
			});
		});
	}
}
