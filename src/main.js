import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

// Ajout logo
document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";

// Set active 'la carte'
const attributePizzaLink = document
	.querySelector('.pizzaListLink')
	.getAttribute('class');
document
	.querySelector('.pizzaListLink')
	.setAttribute('class', `${attributePizzaLink} active`);

// Rendre visible newsContainer
document.querySelector('.newsContainer').setAttribute('style', 'display:');

// closeButton
const button = document.querySelector('.closeButton');
button.addEventListener('click', e => {
	e.preventDefault();
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});

// Menu
const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas
